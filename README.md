> 温馨提示：你可以注册一个码云账号，&#x1F449; 点击右上角的【**Fork**】按钮，可以把这个项目完整复制到你的码云账号下，随时查看。 

&emsp;

# 金融领域中文情绪词典

在大数据时代，越来越多的金融学术研究开始关注上市公司年报、新闻媒体报道和投资者社交媒体发帖等文本中所包含的语调与情绪。情绪词典是测度和构建语调和情绪指标的基础。现有研究使用的情绪词典普遍存在一些问题，例如：使用通用型语言词典而非专业的金融情绪词典，可能导致关键金融情绪词遗漏；使用人工判别方法构建基于小样本的金融情绪词典，可能导致情绪词的判断标准不统一和小样本偏差；直接使用翻译后的英文金融情绪词典，可能导致无法捕捉不同语言类对同一种情绪的不同表达习惯；使用单一类文本样本构造的词典，可能导致无法同时适应正式的金融文件（如新闻、公告、年报等）表达和非正式的网络（如股吧、论坛等）表达这两大类文本中的相同情绪；等等。

本项目通过文本分析和机器学习的方式构建了金融领域中文情绪词典。本词典构建方法具有尽可能避免人工判断、来源于大样本、适用于中文文本表达等优势（详情见下面的参考文献）。本词典针对正式金融文本和社交媒体金融文本的用词差异，分为正式用语情绪词典和非正式用语情绪词典。其中正式用语情绪词典适用于公司年报等正式文本的语调分析，而非正式用语情绪词典则适用于社交媒体等非正式文本的情绪分析。读者如需使用本项目词典，请引用如下参考文献：

姚加权，冯绪，王赞钧，纪荣嵘，张维. 语调、情绪及市场影响：基于金融情绪词典. 管理科学学报，2021. 24(5), 26-46.

# 词典构建方法

（1）正式用语情绪词典构建

利用词典重组方法，在现有广泛使用词典的基础上提炼和构建了适用于金融领域正式文本研究的情绪词典。利用2003-2015年间所有中国上市公司年报文本（共计19970份），结合Engelberg et al.（2012）的语调判断方法区分单个年报的正负面情绪。对三份现有通用型中文情绪词典和Loughran & McDonald（2011）情绪词典的中文翻译版进行词语整合，并加入年报语料的分词结果去重得到初始词典，然后运用带惩罚机制词频法提取情绪词生成正式用语情绪词典。

（2）非正式用语情绪词典构建

利用2011-2016年间雪球论坛用户发帖以及2010-2017年间东方财富网股吧发帖（共计8130多万条发帖），以8789条带有情绪识别符号的股票论坛发帖为训练集，结合长短期记忆模型（Long Short-Term Memory, LSTM）的深度学习算法，并运用带惩罚机制词频法生成了非正式用语情绪词典。

更多词典构建细节请参考论文。

**参考文献**

[1]   姚加权，冯绪，王赞钧，纪荣嵘，张维. 语调、情绪及市场影响：基于金融情绪词典. 管理科学学报，2021. 24(5), 26-46.

[2]   Engelberg, J. E., A. V. Reed, and M. C. Ringgenberg. How Are Shorts Informed? Short Sellers, News, and Information Processing [J]. Journal of Financial Economics. 2012. 105(2), 260-278. 

[3]   Loughran, T., and B. McDonald. When Is A Liability Not A Liability? Textual Analysis, Dictionaries, and 10‐Ks [J]. Journal of Finance. 2011. 66(1), 35-65.

# 附录

<table class="tg">
    <tr>
        <th class="tg-0lax" colspan=15 align="left">部分正式用语情绪词汇：</th>
    </tr>
    <tr>
        <th class="tg-0lax" colspan=15>负面</th>
    </tr>
    <tr>
        <td class="tg-0lax">风险</td>
        <td class="tg-0lax">亏损</td>
        <td class="tg-0lax">违反</td>
        <td class="tg-0lax">损害</td>
        <td class="tg-0lax">舞弊</td>
        <td class="tg-0lax">严重</td>
        <td class="tg-0lax">约束</td>
        <td class="tg-0lax">手段</td>
        <td class="tg-0lax">坏帐</td>
        <td class="tg-0lax">负担</td>
    </tr>
    <tr>
        <td class="tg-0lax">越权</td>
        <td class="tg-0lax">不道德</td>
        <td class="tg-0lax">毁损</td>
        <td class="tg-0lax">异常</td>
        <td class="tg-0lax">谴责</td>
        <td class="tg-0lax">严峻</td>
        <td class="tg-0lax">委靡</td>
        <td class="tg-0lax">困顿</td>
        <td class="tg-0lax">失利</td>
        <td class="tg-0lax">守旧</td>
    </tr>
    <tr>
        <td class="tg-0lax">不健全</td>
        <td class="tg-0lax">仿造</td>
        <td class="tg-0lax">倒闭</td>
        <td class="tg-0lax">侮辱</td>
        <td class="tg-0lax">压制</td>
        <td class="tg-0lax">冒进</td>
        <td class="tg-0lax">刁难</td>
        <td class="tg-0lax">危害</td>
        <td class="tg-0lax">压迫</td>
        <td class="tg-0lax">低迷</td>
    </tr>
    <tr>
        <th class="tg-0lax" colspan=15>正面</th>
    </tr>
    <tr>
        <td class="tg-0lax">平稳</td>
        <td class="tg-0lax">崛起</td>
        <td class="tg-0lax">精神</td>
        <td class="tg-0lax">和谐</td>
        <td class="tg-0lax">突出</td>
        <td class="tg-0lax">合格</td>
        <td class="tg-0lax">力争</td>
        <td class="tg-0lax">透明</td>
        <td class="tg-0lax">成熟</td>
        <td class="tg-0lax">迅速</td>
    </tr>
    <tr>
        <td class="tg-0lax">倾心</td>
        <td class="tg-0lax">保密</td>
        <td class="tg-0lax">清晰</td>
        <td class="tg-0lax">积极性</td>
        <td class="tg-0lax">严正</td>
        <td class="tg-0lax">丰硕</td>
        <td class="tg-0lax">乐观</td>
        <td class="tg-0lax">从优</td>
        <td class="tg-0lax">信誉</td>
        <td class="tg-0lax">充实</td>
    </tr>
    <tr>
        <td class="tg-0lax">不屈</td>
        <td class="tg-0lax">威信</td>
        <td class="tg-0lax">完备</td>
        <td class="tg-0lax">创新</td>
        <td class="tg-0lax">勇气</td>
        <td class="tg-0lax">飙升</td>
        <td class="tg-0lax">富余</td>
        <td class="tg-0lax">干劲</td>
        <td class="tg-0lax">庆祝</td>
        <td class="tg-0lax">强悍</td>
    </tr>
    <tr>
        <th class="tg-0lax" colspan=15 align="left">部分非正式用语情绪词汇：</th>
    </tr>
    <tr>
        <th class="tg-0lax" colspan=15>负面</th>
    </tr>
    <tr>
        <td class="tg-0lax">垃圾</td>
        <td class="tg-0lax">下跌</td>
        <td class="tg-0lax">回调</td>
        <td class="tg-0lax">割肉</td>
        <td class="tg-0lax">套牢</td>
        <td class="tg-0lax">风险</td>
        <td class="tg-0lax">减持</td>
        <td class="tg-0lax">抛售</td>
        <td class="tg-0lax">可悲</td>
        <td class="tg-0lax">低迷</td>
    </tr>
    <tr>
        <td class="tg-0lax">向下</td>
        <td class="tg-0lax">跌破</td>
        <td class="tg-0lax">无耻</td>
        <td class="tg-0lax">狗屎</td>
        <td class="tg-0lax">利空</td>
        <td class="tg-0lax">困顿</td>
        <td class="tg-0lax">可笑</td>
        <td class="tg-0lax">跳空</td>
        <td class="tg-0lax">倒霉</td>
        <td class="tg-0lax">赔钱</td>
    </tr>
    <tr>
        <td class="tg-0lax">烂股</td>
        <td class="tg-0lax">小人</td>
        <td class="tg-0lax">绝望</td>
        <td class="tg-0lax">卑鄙</td>
        <td class="tg-0lax">压制</td>
        <td class="tg-0lax">不值</td>
        <td class="tg-0lax">草包</td>
        <td class="tg-0lax">担心</td>
        <td class="tg-0lax">丢脸</td>
        <td class="tg-0lax">烦心</td>
    </tr>
    <tr>
        <th class="tg-0lax" colspan=15>正面</th>
    </tr>
    <tr>
        <td class="tg-0lax">涨停</td>
        <td class="tg-0lax">崛起</td>
        <td class="tg-0lax">胜利</td>
        <td class="tg-0lax">献花</td>
        <td class="tg-0lax">发财</td>
        <td class="tg-0lax">暴涨</td>
        <td class="tg-0lax">战斗机</td>
        <td class="tg-0lax">稳赚</td>
        <td class="tg-0lax">过瘾</td>
        <td class="tg-0lax">幸运</td>
    </tr>
    <tr>
        <td class="tg-0lax">黑马</td>
        <td class="tg-0lax">赚翻天</td>
        <td class="tg-0lax">爽歪歪</td>
        <td class="tg-0lax">止跌</td>
        <td class="tg-0lax">恭喜</td>
        <td class="tg-0lax">开心</td>
        <td class="tg-0lax">舒服</td>
        <td class="tg-0lax">漂亮</td>
        <td class="tg-0lax">牛股</td>
        <td class="tg-0lax">完美</td>
    </tr>
    <tr>
        <td class="tg-0lax">赚大</td>
        <td class="tg-0lax">期待</td>
        <td class="tg-0lax">好样</td>
        <td class="tg-0lax">创新</td>
        <td class="tg-0lax">勇气</td>
        <td class="tg-0lax">神奇</td>
        <td class="tg-0lax">明智</td>
        <td class="tg-0lax">成功</td>
        <td class="tg-0lax">飙升</td>
        <td class="tg-0lax">支持</td>
    </tr>
</table>


<br>
<br>



&emsp;
 
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> Stata连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html) || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) || [Bilibili 站](https://space.bilibili.com/546535876)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-草料主页-一码平川600.png)

&emsp;

## 1. 连享会课程

> **免费公开课：**    
- [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)，[B 站版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [B 站版](https://space.bilibili.com/546535876/channel/detail?cid=160748)   
- [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin) 
- 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等) 

&emsp;

> ### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)](https://www.lianxh.cn/news/46917f1076104.html)

> ### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 2. 资源分享

### 视频公开课

- [连享会码云：100多个精选计量项目](https://www.lianxh.cn/news/944a69d75cec9.html) |  [新浪视频](https://weibo.com/tv/show/1034:4479228373303338)
- [五分钟 Markdown / Markdown 幻灯片](https://gitee.com/arlionn/md) | [新浪视频](https://weibo.com/tv/show/1034:4484204327796746)
- [连老师给你的-听课建议](https://www.lianxh.cn/news/69706e871c9ad.html)
- [直击面板数据模型](http://lianxh-pc.duanshu.com/course/detail/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[B 站版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/arlionn/stata101)，[课件](https://gitee.com/arlionn/stata101)，[B 站版](https://space.bilibili.com/546535876/channel/detail?cid=160748)    
- [Stata小白的取经之路](https://gitee.com/arlionn/StataBin)，龙志能，上海财经大学，[去听课](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) 

### Stata

- [连享会推文](https://www.lianxh.cn) | [直播视频](http://lianxh.duanshu.com)
- **计量专题课程**: [Stata暑期班/寒假班](https://gitee.com/lianxh/text) | [专题课程](https://gitee.com/arlionn/Course)
- Stata专栏：[最新推文](https://www.lianxh.cn) | [知乎](https://www.zhihu.com/people/arlionn/) | [CSDN](https://blog.csdn.net/arlionn) | [Bilibili 站](https://space.bilibili.com/546535876)
- Books and Journal: [计量Books](https://quqi.gblhgk.com/s/880197/hmpmu2ylAcvHnXwY) | [SJ-PDF](https://quqi.gblhgk.com/s/880197/eipgoUi6Gd1FDZRu) | [Stata Journal-在线浏览](https://www.lianxh.cn/news/12ffe67d8d8fb.html)
- Stata Guys：[Ben Jann](http://www.soz.unibe.ch/about_us/personen/prof_dr_jann_ben/index_eng.html) 

### Data
- [CSMAR-国泰安](http://www.gtarsc.com/#/datacenter/singletable) | [Wind-万德](https://www.wind.com.cn/Default.html) | [Resset-锐思](http://www.resset.cn/databases)
- [常用数据库](https://www.lianxh.cn/news/0b65fd5165c2c.html) 
- [人文社科开放数据库](https://www.lianxh.cn/news/6f06c914acde8.html) 
- [徐现祥教授-IRE-官员交流、方言等](https://www.lianxh.cn/news/8c9f81a5f19ee.html)
- [知乎-Data](https://www.zhihu.com/question/20179699/answer/681756635)

### Papers - 学术论文复现
- [论文重现网站](https://www.lianxh.cn/news/e87e5976686d5.html)
- [Google学术](https://ac.scmor.com/) | [统一入口：虫部落学术搜索](http://scholar.chongbuluo.com/) | [微软学术](https://academic.microsoft.com/home)
- [iData - 期刊论文下载](https://www.cn-ki.net/)
- [ CNKI ](http://scholar.cnki.net/) | [百度学术](http://xueshu.baidu.com/) | [Google学术](https://scholar.glgoo.org/) | [Sci-hub ](http://www.sci-hub.cc/), [2](http://sci-hub.ac/), [3](http://sci-hub.bz/), [4](http://sci-hub.ac/)
- Stata论文重现:  [Harvard dataverse][harvd] | [JFE][jfe]  | [github][git1] | [Yahoo-github][yahoogit]
- 学者主页(提供了诸多论文的原始数据和 dofiles)：[Angrist][Ang1] || [Daron Acemoglu][acem]  || [Ross Levine][ross] || [Esther Duflo][Duflo] || [Imbens](https://scholar.harvard.edu/imbens/software)  ||  [Raj Chetty](http://www.rajchetty.com/)

[harvd]:https://dataverse.harvard.edu/dataverse
[jfe]:http://jfe.rochester.edu/data.htm
[Ang1]:http://economics.mit.edu/faculty/angrist/data1/data
[acem]:http://economics.mit.edu/faculty/acemoglu/data
[ross]:http://faculty.haas.berkeley.edu/ross_levine/papers.htm
[duflo]:http://economics.mit.edu/faculty/eduflo/papers
[git1]:https://github.com/search?utf8=%E2%9C%93&q=stata&type=

[yahoogit]:https://search.yahoo.com/search;_ylt=AwrBT8di2LBZqyEAuG9XNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3lmcC10LTQ3MwRncHJpZAMEbl9yc2x0AzAEbl9zdWdnAzAEb3JpZ2luA3NlYXJjaC55YWhvby5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMwBHFzdHJsAzE0BHF1ZXJ5A3N0YXRhJTIwZ2l0aHViBHRfc3RtcAMxNTA0NzYxODcz?p=stata+github&fr2=sb-top&fr=yfp-t-473&fp=1

&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，500+ 推文，实证分析不再抓狂；[Bilibili 站](https://space.bilibili.com/546535876) 有视频大餐。

&emsp; 

> &#x26F3;  **`lianxh` 命令发布了：**    
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;



